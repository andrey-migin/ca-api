using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Web.Services;
using ILogger = Serilog.ILogger;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private const string SeqLogUrl = "SEQ_URL";

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var seqUrl = GetEnvVariable(SeqLogUrl, false);

            services.AddMvc(o => { o.EnableEndpointRouting = false; });

            services.AddSwaggerDocument(o => o.Title = "CA api");


            if (seqUrl != null)
            {
                services.AddSingleton<ILogger>( new LoggerConfiguration()
                    .Enrich.WithProperty("AppName", "CaApi").WriteTo.Seq(seqUrl).CreateLogger());
            }
            else
            {
                Console.WriteLine($"{SeqLogUrl} env variable is empty. Logging is going to go to the console");
                services.AddLogging(builder =>
                {
                    services.AddSingleton<ILogger>(new LoggerConfiguration()
                        .Enrich.WithProperty("AppName", "CaApi").CreateLogger());

                });
            }

            services.AddHttpContextAccessor();
            services.AddScoped<AlertService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {

            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseEndpoints(
                endpoints => { endpoints.MapControllers(); });
        }


        private static string GetEnvVariable(string name, bool required = true)
        {
            var result = Environment.GetEnvironmentVariable(name);
            if (result == null && required)
                throw new Exception($"Env variable is not found with name: {name}");

            return result;
        }
        
    }
    
}