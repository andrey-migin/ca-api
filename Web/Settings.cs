using System;

namespace Web
{
    public static class Settings
    {

        private static string GetEnvVariable(string envName)
        {
            var result = Environment.GetEnvironmentVariable(envName);
            
            if (string.IsNullOrEmpty(result))
                throw new Exception("Env variable '"+envName+"' is not found");

            return result;
        }

        public static string GetCompanyName()
        {
            return GetEnvVariable("COMPANY_NAME");
        }
        
        
        public static string GetCountryIso2Code()
        {
            return GetEnvVariable("COUNTRY_ISO2");
        }
        
        public static string GetProvince()
        {
            return GetEnvVariable("PROVINCE");
        }
        
        public static string GetCity()
        {
            return GetEnvVariable("CITY");
        }
        
        public static string GetLegalName()
        {
            return GetEnvVariable("LEGAL_NAME");
        }
        
        public static string GetEmail()
        {
            return GetEnvVariable("EMAIL");
        }

        
    }
}