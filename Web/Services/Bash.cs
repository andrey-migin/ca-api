using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Services
{
    public static class Bash
    {


        private static string GetFolderFromEnvVariable(string envVariable)
        {
            var result = Environment.GetEnvironmentVariable(envVariable);

            if (string.IsNullOrEmpty(result))
                result = Home.Value;

            if (result.StartsWith("~"))
                result = result.Replace("~", Home.Value);

            if (result.Last() != Path.DirectorySeparatorChar)
                return result + Path.DirectorySeparatorChar;

            return result;
        }
        
        
        private static readonly Lazy<string> Home = new Lazy<string>(
            () => Environment.GetEnvironmentVariable("HOME") ?? new string(new[] {Path.DirectorySeparatorChar}));


        private static readonly Lazy<string> WorkingFolder =
            new Lazy<string>(() => GetFolderFromEnvVariable("SCRIPTFOLDER"));
        
        private static readonly Lazy<string> OutputFolder =
            new Lazy<string>(() => GetFolderFromEnvVariable("OUTPUTFOLDER"));


        public const string ConfigFile = "/root/easy-rsa/vars";
        
        public static string GetWorkingFolder()
        {
            return WorkingFolder.Value;
        }


        public static byte[] ToUtf8Bytes(this string line)
        {
            return Encoding.UTF8.GetBytes(line);
        }

        private static ValueTask WriteStringLineAsync(this Stream stream,  string line)
        {
            return stream.WriteAsync((line+"\n").ToUtf8Bytes());
        }

        private static async Task SaveVarFile(string fileName)
        {
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                await fileStream.WriteStringLineAsync("set_var EASYRSA_DN \"org\"");
                await fileStream.WriteStringLineAsync($"set_var EASYRSA_REQ_CN		\"{Settings.GetCompanyName()}\"");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_KEY_SIZE 4096");
                await fileStream.WriteStringLineAsync($"set_var EASYRSA_REQ_COUNTRY \"{Settings.GetCountryIso2Code()}\"");
                await fileStream.WriteStringLineAsync($"set_var EASYRSA_REQ_PROVINCE \"{Settings.GetProvince()}\"");
                await fileStream.WriteStringLineAsync($"set_var EASYRSA_REQ_CITY \"{Settings.GetCity()}\"");
                await fileStream.WriteStringLineAsync($"set_var EASYRSA_REQ_ORG \"{Settings.GetLegalName()}\"");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_REQ_OU \"IT\"");
                await fileStream.WriteStringLineAsync($"set_var EASYRSA_REQ_EMAIL \"{Settings.GetEmail()}\"");
                await fileStream.WriteStringLineAsync("set_var EASYRSA	\"/root/easy-rsa\"");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_PKI	\"$EASYRSA/keys\"");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_CA_EXPIRE	3650");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_CERT_EXPIRE	3650");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_SSL_CONF	\"$EASYRSA/openssl-1.0.cnf\"");
                await fileStream.WriteStringLineAsync("set_var EASYRSA_OPENSSL	\"openssl\"");       
                await fileStream.WriteStringLineAsync("set_var EASYRSA_EXT_DIR	\"$EASYRSA/x509-types\"");       
                await fileStream.WriteStringLineAsync("set_var EASYRSA_EXT_DIR	\"$EASYRSA/x509-types\"");       
                await fileStream.WriteStringLineAsync("set_var EASYRSA_CRL_DAYS	\"3650\"");       
                await fileStream.WriteStringLineAsync("set_var EASYRSA_DIGEST	\"sha256\"");       
            }
        }


        public static string GeneratePfxFilename(this string email)
        {
            return "/root/easy-rsa/keys/private/" + email + ".pfx";
        }
        
        public static async Task CreateVarFileIfNeededAsync()
        {
            await SaveVarFile(ConfigFile);
            await SaveVarFile(GetWorkingFolder()+"easy-rsa/vars");
            
            File.Copy(GetWorkingFolder()+"easy-rsa/openssl-1.0.cnf","/root/easy-rsa/openssl-1.0.cnf");


            if (!Directory.Exists("/root/easy-rsa/x509-types"))
                Directory.CreateDirectory("/root/easy-rsa/x509-types");
            
            File.Copy(GetWorkingFolder()+"easy-rsa/x509-types/ca","/root/easy-rsa/x509-types/ca");
            File.Copy(GetWorkingFolder()+"easy-rsa/x509-types/client","/root/easy-rsa/x509-types/client");
            File.Copy(GetWorkingFolder()+"easy-rsa/x509-types/COMMON","/root/easy-rsa/x509-types/COMMON");
            File.Copy(GetWorkingFolder()+"easy-rsa/x509-types/server","/root/easy-rsa/x509-types/server");
            
        }



        private static string GetExecutionParameters(this string[] data)
        {
            var result = new StringBuilder();

            foreach (var param in data.Skip(1))
            {
                result.Append(param + ' ');
            }

            return result.ToString().Trim();
        }

        public static async Task<BashExecutionResult> RunScriptAsync(this string cmd)
        {
            

            var lines = cmd.Split(' ');

            var fileName = lines[0];

            var workingDir = Path.GetDirectoryName(fileName);
            fileName = Path.GetFileName(fileName);
            
            var escapedArgs = lines.GetExecutionParameters().Replace("\"", "\\\"");
            
            Console.WriteLine("Executing: "+cmd);
            
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = fileName,
                    Arguments = escapedArgs,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = workingDir,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    
                }
            };
            process.Start();
            await process.WaitForExitAsync();
            
            var okResult = await process.ReadOkResultAsync();
            var errorResult = await process.ReadErrorResultAsync();
            
            Console.WriteLine();

            return new BashExecutionResult
            {
                BashExitCode = process.ExitCode,
                OkResult = okResult,
                ErrorResult = errorResult
            };
        }

        private static async Task<string> ReadOkResultAsync(this Process process)
        {
            try
            {
                return await process.StandardOutput.ReadToEndAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "Could not read result from the Stream";
            }
        }
        
        private static async Task<string> ReadErrorResultAsync(this Process process)
        {
            try
            {
                return await process.StandardError.ReadToEndAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "Could not read result from the Stream";
            }
        }
        
        public static async Task<BashExecutionResult> RunOpenSslAsync(this string args)
        {
            var escapedArgs = args.Replace("\"", "\\\"");

            Console.WriteLine("Executing: "+escapedArgs);
            
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "openssl",
                    Arguments = escapedArgs,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    WorkingDirectory = "/usr/bin"
                    
                }
            };
            process.Start();

            await process.WaitForExitAsync();
            
            var okResult = await process.ReadOkResultAsync();
            
            var errorResult = await process.ReadErrorResultAsync();
            
            Console.WriteLine();

            return new BashExecutionResult
            {
                BashExitCode = process.ExitCode,
                OkResult = okResult,
                ErrorResult = errorResult
            };
        }


    }
}