using System.IO;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Services
{
    public static class BashCommands
    {


        public static async Task<BashExecutionResult> GenerateCa()
        {
            await Bash.CreateVarFileIfNeededAsync();
            var cmd = "CreateClientCertsCA.sh";
            return await cmd.RunScriptAsync();
        }
        
        public static async Task<BashExecutionResult> GeneratePfxFileAsync(this string email, string password)
        {
            var fileName = email.GeneratePfxFilename();

            if (File.Exists(fileName))
                File.Delete(fileName);

            var openSslArgs =
                $"pkcs12 -inkey /root/easy-rsa/keys/private/{email}.key -in /root/easy-rsa/keys/issued/{email}.crt -export -out /root/easy-rsa/keys/private/{email}.pfx -password pass:{password}";
            
            return await openSslArgs.RunOpenSslAsync();
        }

        public static async Task<BashExecutionResult> GenerateClientCert(string email, string password)
        {
            var cmd = $"/root/easy-rsa/easyrsa.real build-client-full {email} nopass";
            await cmd.RunScriptAsync();

            return await email.GeneratePfxFileAsync(password);
        }
        
                
        public static async Task<BashExecutionResult> RevokeCert(string email)
        {
            var cmd =
                $"ca -config /root/easy-rsa/openssl-1.0.cnf -revoke /root/easy-rsa/keys/issued/{email}.crt";

            return await cmd.RunOpenSslAsync();
        }
        
        public static async Task<BashExecutionResult> BuildCrlCert()
        {
            const string revokeCertCommand = "ca -config /root/easy-rsa/openssl-1.0.cnf -gencrl -crldays 3650 -out /root/crl/crl.pem";
            return await revokeCertCommand.RunOpenSslAsync();
        }
        
        
    }
}