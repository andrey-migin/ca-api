using System;
using Microsoft.AspNetCore.Http;
using Serilog;
using Web.Controllers;

namespace Web.Services
{
    public enum Operations
    {
        CaCreate,
        ClientCertCreate,
        PfxCreate,
        Revoke,
        Download
        
    }
    public class AlertService 
    {
        private readonly IHttpContextAccessor _ctx;
        private readonly ILogger _logger;
        
        public AlertService(IHttpContextAccessor ctx, ILogger logger)
        {
            _ctx = ctx;
            _logger = logger;
        }
        
        public void Alert(Operations operation, string forEmail)
        {
            var message =
                $"{_ctx.HttpContext.GetUserByCn()} {operation.ToString()} for {forEmail}.\n {DateTime.UtcNow:MM/dd/yyyy hh:mm:ss.fff}";

            try
            {
                _logger.Information(message);
                //await _slackApi.PostMessageAsync("#ca-api-auditlog", message);
            }
            catch (Exception e)
            {
                _logger.Error(e, e.Message);
            }
        }
    }
}