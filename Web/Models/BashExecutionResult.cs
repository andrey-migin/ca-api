namespace Web.Models
{
    public class BashExecutionResult
    {
        public int BashExitCode { get; set; }
        public string OkResult { get; set; }
        public string ErrorResult { get; set; }
    }
}