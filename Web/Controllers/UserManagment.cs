using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Web.Controllers
{
    public static class UserManagement
    {
        private const string AuthHeader = "X-User";
        private const string SslCertCnHeader = "x-ssl-client-s-dn";
        public static string GetUserId(this HttpContext ctx)
        {
            if (ctx.Request.Headers.ContainsKey("AuthHeader"))
                return ctx.Request.Headers[AuthHeader];
            
            throw new Exception("Un-authorized request");
        }

        public static string GetUserByCn(this HttpContext ctx)
        {
            return !ctx.Request.Headers.ContainsKey(SslCertCnHeader) ? null 
                : ctx.Request.Headers[SslCertCnHeader].ToString().Split("=").Last();
        }
            
    }

}