using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Services;

namespace Web.Controllers
{
    public class DownloadController : Controller
    {
        private readonly AlertService _alertService;
        public DownloadController(AlertService alertService)
        {
            _alertService = alertService;
        }
        
        [HttpGet("/Download/{email}")]
        public async Task<IActionResult> Index(string email)
        {
            var content = await System.IO.File.ReadAllBytesAsync(email.GeneratePfxFilename());
            _alertService.Alert(Operations.Download, email);
            return File(content, "application/x-binary", email + ".pfx");
        }
    }
}