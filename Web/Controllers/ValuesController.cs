﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[action]")]
    [ApiController]
    public class CertsController : ControllerBase
    {
        private readonly AlertService _alertService;
        public CertsController(AlertService alertService)
        {
            _alertService = alertService;
        }
        
        // GET api/values
        [HttpPost]
        public async Task<BashExecutionResult> GenerateCA()
        {
            return await BashCommands.GenerateCa();
        }
        
        
        [HttpPost]
        public async Task<BashExecutionResult> GenerateClientCert([FromForm]string email, [Required][FromForm]string password)
        {
            var result = await BashCommands.GenerateClientCert(email, password);
            _alertService.Alert(Operations.ClientCertCreate, email);
            return result;
        }
        
        
        [HttpPost]
        public async Task<BashExecutionResult> GeneratePfx([FromForm]string email, [Required][FromForm]string password)
        {
            var result = await email.GeneratePfxFileAsync(password);
            _alertService.Alert(Operations.PfxCreate, email);
            return result;
        }

        [HttpDelete]
        public async Task<BashExecutionResult> RevokeClientCert([FromForm] string email)
        {
            var result = await BashCommands.RevokeCert(email);
            _alertService.Alert(Operations.Revoke, email);
            return result;
        }
        
        [HttpPost]
        public async Task<BashExecutionResult> BuildCrlPm()
        {
            return await BashCommands.BuildCrlCert();
        }
    }
}

